<?php

    require ("animal.php");
    require ("ape.php");
    require ("frog.php");
    
    $sheep = new animal("shaun");

    echo 'Nama hewan : ' . $sheep->name . '<br>';
    echo 'Jumlah kaki : ' . $sheep->legs . '<br>';
    echo 'Berdarah Dingin : ' . $sheep->cold_blooded . '<br><br>';

    $sungokong = new Ape("kera sakti");
    
    echo 'Nama hewan : ' . $sungokong->name . '<br>';
    echo 'Jumlah kaki : ' . $sungokong->legs . '<br>';
    echo 'Berdarah Dingin : ' . $sungokong->cold_blooded . '<br>';
    $sungokong->yell();

    $kodok = new frog("buduk");
    
    echo 'Nama hewan : ' . $kodok->name . '<br>';
    echo 'Jumlah kaki : ' . $kodok->legs . '<br>';
    echo 'Berdarah Dingin : ' . $kodok->cold_blooded . '<br>';
    $kodok->jump();